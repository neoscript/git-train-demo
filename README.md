# Git教程大纲
1. Git介绍  
    * [版本库管理系统类型介绍](01.git-basic/01.01.cvs-basic.md)
    * [Git原理，workdir、stage、repository介绍](01.git-basic/01.02.git-basics.md)
    * [Git安装-Windows命令行](01.git-basic/01.03.git-setup-windows.md)
    * [Git安装-Linux](01.git-basic/01.04.git-setup-linux.md)
1. [Git版本库的初始化和SVN版本库的导入](02.git-init/02.01.git-init-clone-svn.md)
1. Git基础命令
    * [add, status, commit, diff, show](03.base-cmd/03.01.base-cmd-add-diff.md)
    * [log, reset, tag](03.base-cmd/03.02.base-cmd-log-reset-tag.md)
    * [config and alias](03.base-cmd/03.03.base-cmd-config-alias.md)
    * [remote, push, pull, clone](03.base-cmd/03.04.base-cmd-remote.md)
1. [Git作为运维工具实践](04.upgrade-demo/04.01.upgrade-demo.md)
1. Git分支管理
    * [branch, merge, checkout](05.branch/05.01.branch-checkout.md)
    * [reset, revert](05.branch/05.02.reset-revert.md)
    * [rebase](05.branch/05.03.rebase.md)
    * [分支管理流程](05.branch/05.04.branch-manage.md)
1. Git其它常用命令
    * [stash](06.file-cmd/06.01.stash.md)
    * [clean, grep, bisect](06.file-cmd/06.02.clean-grep.md)
1. Gitlab演示
    * [规范说明](07.gitlab/07.01.gitlab-rule.md)
    * [工程、组、权限管理](07.gitlab/07.02.project-manage.md)
    * [Markdown,Wiki介绍](07.gitlab/07.03.markdown-wiki.md)
1. 自学可视化操作：[TortoiseGit](http://130.1.108.17:9090/downloads/git-install/TortoiseGit-2.3.0.0-64bit.msi)、Eclipse、IntellJIDEA。

# 上机操作环境准备
* 注册[码云](https://gitee.com)、[Github](https://github.com)或[Gitlab](https://gitlab.com)帐号;
* Git客户端下载安装
    1. **Windows**：下载 [cmder1.3.2_full.7z](http://130.1.108.17:9090/downloads/git-install/cmder1.3.2_full.7z) 并解压运行Cmder.exe；
    1. **Linux**：`yum install git`
* Git远程库
    1. 操作人员自行准备一台Linux服务器作为Git远程库；
    1. 需要通过root用户[安装Linux的Git客户端](01.git-basic/01.04.git-setup-linux.md)；
    1. 安装后可使用任意用户建立Git版本库，只需目录读写和ssh访问权限。
    1. 新建工程-[码云](https://gitee.com)、[Github](https://github.com)或[Gitlab](https://gitlab.com)
