### Gitlab注册规范
`请注意不符合规范的帐户将被定期清理，包括上传的代码！`

1. Name 中文真名
1. Username 拼音全拼，如有冲突加公司简拼为后缀，如xiaoming_ibm
1. Email username@公司邮箱(白名单限制)
1. 注册成功后进入[个人设置页面](http://git.beeb.com/profile)，将"Linkedin"属性设为工号、外包人员编号。

### Project Group命名规范
1. 全部为小写单词
1. 多个单词中间用`-`分隔
1. 例如：`beeb-train`, `git-train-demo`

### 邮箱白名单
>**beeb.com.cn**, huateng.com, yuxinyicheng.com, gaoweida.com, kelan.com, tianzheng.com, huawei.com, zantong.com, cloudcore.cn, yongyou.com, youchuang.com, yinfeng.com, baifendian.com,feelingtech.net

### 系统参数说明
1. 文件大小限制：10M， 非源码文件请通过其它方式保存 。
1. 可建工程数量： 100 。

### Git学习教程
* [Demo工程](http://git.beeb.com/beeb-train/git-train-demo)
* [Wiki链接](http://git.beeb.com/beeb-train/git-train-demo/wikis/home)

### 其它注意事项
1. `Git版本库为分布式，本地目录包含所有日志，所以服务器上不再做全量备份，各项目组可按照QA要求打包本地文件夹使用FTP等方式做集中备份。`
